package com.example.allrighttestproject.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.allrighttestproject.data.model.MatchingExercise
import com.example.allrighttestproject.data.model.tableName
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface ExerciseDao {

    @Query("SELECT * FROM $tableName")
    fun getAllExercises(): Flowable<List<MatchingExercise>>

    @Query("SELECT * FROM $tableName LIMIT :amount")
    fun getNumberOfRandomExercises(amount: Int): Flowable<List<MatchingExercise>>

    @Insert
    fun addExercises(list: List<MatchingExercise>): Completable

}