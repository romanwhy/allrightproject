package com.example.allrighttestproject.data.model

import androidx.annotation.DrawableRes
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val tableName = "exercise"

@Entity(tableName = tableName)
data class MatchingExercise(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(index = true)
    val id: Int? = null,
    @ColumnInfo(name = "picture_res_id")
    @DrawableRes val pictureResId: Int,
    val label: String
)