package com.example.allrighttestproject.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.allrighttestproject.data.model.MatchingExercise

const val DATABASE_NAME = "Exercise.db"

@Database(entities = [MatchingExercise::class], version = 1)
abstract class ExerciseDatabase : RoomDatabase() {

    abstract fun exerciseDao(): ExerciseDao

}