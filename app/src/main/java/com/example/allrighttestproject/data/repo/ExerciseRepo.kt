package com.example.allrighttestproject.data.repo

import com.example.allrighttestproject.data.database.ExerciseDao
import com.example.allrighttestproject.data.model.MatchingExercise
import com.example.allrighttestproject.presentation.util.RxSchedulers
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

class ExerciseRepo @Inject constructor(
    private val dao: ExerciseDao,
    private val schedulers: RxSchedulers,
    @Named("databaseEntries") private val initialEntries: List<MatchingExercise>
) {

    /**
     * Get list of exercises, populate with initial values if it is empty
     */
    fun getListOfExercises(count: Int) =
        fetchExercises(count)
            .take(1)
            .filter { list ->
                list.isNotEmpty()
            }
            .switchIfEmpty(addExercisesAndFetch(count = count))

    private fun fetchExercises(count: Int) = dao.getNumberOfRandomExercises(count).compose(schedulers.flowable())

    private fun addExercisesAndFetch(list: List<MatchingExercise> = initialEntries, count: Int) =
        dao
            .addExercises(list)
            .doOnComplete {
                Timber.d("fuck")

            }
            .compose(schedulers.completable())
            .andThen(fetchExercises(count))

}
