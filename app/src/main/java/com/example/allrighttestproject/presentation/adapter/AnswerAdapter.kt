package com.example.allrighttestproject.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.allrighttestproject.databinding.ItemWordBinding
import com.example.allrighttestproject.domain.model.AnswerViewEntity
import com.example.allrighttestproject.presentation.util.BindingViewHolder

class AnswerAdapter(
    callback: DiffUtil.ItemCallback<AnswerViewEntity>,
    val onAnswerSelected: (index: Int) -> Unit
) : ListAdapter<AnswerViewEntity, BindingViewHolder>(callback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemWordBinding.inflate(inflater)
        val vh = BindingViewHolder(binding)
        with(binding) {
            viewTest.setOnClickListener {
                if (vh.adapterPosition != RecyclerView.NO_POSITION) {
                    onAnswerSelected(vh.adapterPosition)
                }
            }
        }
        return vh
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int, payloads: MutableList<Any>) {
        //do nothing
        super.onBindViewHolder(holder, position, payloads)
    }
}