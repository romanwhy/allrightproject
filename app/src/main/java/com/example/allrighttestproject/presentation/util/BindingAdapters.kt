package com.example.allrighttestproject.presentation.util

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.allrighttestproject.BR


@BindingAdapter("src")
fun ImageView.setImage(resId: Int) {
    Glide.with(this).load(resId).into(this)
}

fun setLayoutWidth(view: View, width: Float) {
    val params = view.layoutParams
    params.width = width.toInt()
    view.layoutParams = params
}

class BindingViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(itemModel: Any, key: Int = BR.listItem) {
        binding.setVariable(key, itemModel)
    }

}
