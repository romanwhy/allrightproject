package com.example.allrighttestproject.presentation.view

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.os.postDelayed
import androidx.core.view.doOnPreDraw
import androidx.core.view.forEach
import androidx.core.view.get
import androidx.dynamicanimation.animation.DynamicAnimation
import androidx.dynamicanimation.animation.FloatPropertyCompat
import androidx.dynamicanimation.animation.SpringAnimation
import androidx.dynamicanimation.animation.SpringForce.*
import androidx.transition.Fade
import androidx.transition.Fade.IN
import androidx.transition.TransitionManager
import androidx.transition.Visibility
import com.example.allrighttestproject.R
import com.example.allrighttestproject.databinding.WordOptionViewBinding
import com.example.allrighttestproject.presentation.util.setImage
import timber.log.Timber
import kotlin.math.hypot
import kotlin.random.Random

class WordOptionView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    enum class State {
        //empty
        NOT_SET,
        //default
        SET,
        //yellow
        SELECTED,
        //green
        MATCHED,
        //red
        NOT_MATCHED
    }

    private val binding: WordOptionViewBinding =
        WordOptionViewBinding.inflate(LayoutInflater.from(context), this, true)

    private var previousState: State? = null

    var state: State = State.SET
        set(value) {
            if (field == value) return
            previousState = field
            field = value
            when (value) {
                State.SELECTED -> setSelected()
                State.MATCHED -> setMatched()
                State.NOT_MATCHED -> setNotMatched()
                State.NOT_SET -> setEmpty()
                State.SET -> setDefault()
            }
        }

    private fun setEmpty() {
        with(binding) {
            overlay.visibility = View.INVISIBLE
            label.visibility = View.INVISIBLE
            icon.visibility = GONE
            borders.visibility = View.VISIBLE
        }
    }


    private fun setDefault() {
        previousState?.let {
            //no animation in this case looks better
            if (it == State.SELECTED) {
                binding.root.backgroundTintList = ContextCompat.getColorStateList(context, R.color.colorWordDefault)
                return
            } else if (it == State.NOT_SET) {
                binding.icon.visibility = GONE
                binding.label.visibility = View.VISIBLE
                binding.borders.visibility = View.INVISIBLE

                //animate dramatic appearance
                val props = object : FloatPropertyCompat<ViewGroup>("scale") {
                    override fun setValue(vg: ViewGroup, value: Float) {
                        vg.forEach { view ->
                            view.scaleX = value
                            view.scaleY = value
                        }
                    }

                    override fun getValue(vg: ViewGroup): Float =
                        vg.get(0).scaleX
                }

                //make a scale up animation
                SpringAnimation(binding.root as ViewGroup, props, 1f).apply {
                    getSpring()
                        .setDampingRatio(DAMPING_RATIO_MEDIUM_BOUNCY)
                        .setStiffness(STIFFNESS_MEDIUM)
                    setMinimumVisibleChange(DynamicAnimation.MIN_VISIBLE_CHANGE_SCALE)
                    setStartValue(0.1f).start()
                }
                return
            }
        }
        changeOverlayColor(R.color.colorWordDefault)
    }

    private fun setMatched() {
        changeOverlayColor(R.color.colorWordMatch)
        showIcon(R.drawable.ic_check)
    }

    private fun showIcon(@DrawableRes drawableRes: Int) {
        with(binding) {
            icon.setImage(drawableRes)
            val transition = Fade(IN).addTarget(icon)
            TransitionManager.beginDelayedTransition(root as ViewGroup, transition)
            icon.visibility = View.VISIBLE
        }
    }

    private fun setNotMatched() {
        changeOverlayColor(R.color.colorWordNoMatch)
        showIcon(R.drawable.ic_wrong)
    }

    private fun changeOverlayColor(@ColorRes color: Int) {
        with(binding) {
            label.visibility = View.VISIBLE
            borders.visibility = View.INVISIBLE
            icon.visibility = View.GONE
            animateBackground(startAction = {
                overlay.backgroundTintList = ContextCompat.getColorStateList(context, color)
            }, endAction = {
                root.backgroundTintList = ContextCompat.getColorStateList(context, color)
            })
        }
    }

    private fun animateBackground(startAction: () -> Unit, endAction: () -> Unit = {}) {
        with(binding) {
            overlay.visibility = View.INVISIBLE
            overlay.doOnPreDraw {
                overlay.visibility = View.VISIBLE
                ViewAnimationUtils.createCircularReveal(
                    overlay,
                    overlay.width / 2,
                    overlay.height / 2,
                    0f,
                    hypot(overlay.width.toDouble(), overlay.height.toDouble()).toFloat()
                ).apply {
                    addListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationStart(animation: Animator?) {
                            super.onAnimationStart(animation)
                            startAction()
                        }

                        override fun onAnimationEnd(animation: Animator?) {
                            super.onAnimationEnd(animation)
                            endAction()
                            overlay.visibility = View.GONE
                        }
                    })
                    start()
                }
            }
        }
    }

    private fun setSelected() {
        changeOverlayColor(R.color.colorSelectedWordBg)
    }

    fun setText(text: String) {
        binding.label.text = text
    }

}