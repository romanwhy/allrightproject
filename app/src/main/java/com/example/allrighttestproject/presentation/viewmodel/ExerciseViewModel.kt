package com.example.allrighttestproject.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.allrighttestproject.R
import com.example.allrighttestproject.domain.model.AnswerViewEntity
import com.example.allrighttestproject.domain.model.ExerciseViewEntity
import com.example.allrighttestproject.domain.usecase.GetExerciseListUseCase
import com.example.allrighttestproject.presentation.util.StringProvider
import com.example.allrighttestproject.presentation.util.liveData
import com.example.allrighttestproject.presentation.view.WordOptionView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class ExerciseViewModel @Inject constructor(
    private val useCase: GetExerciseListUseCase,
    private val stringProvider: StringProvider
) : ViewModel() {

    private val disposables = CompositeDisposable()

    private val _exerciseList = MutableLiveData<List<ExerciseViewEntity>>()
    val exerciseList by liveData(_exerciseList)

    private val _answerList = MutableLiveData<List<AnswerViewEntity>>()
    val answerList by liveData(_answerList)

    private val _checkResult = MutableLiveData<CheckResult>()
    val checkResult by liveData(_checkResult)

    val buttonText = Transformations.map(checkResult) {
        val stringId = when (it) {
            CheckResult.SUCCESS -> R.string.repeat
            CheckResult.TRY_AGAIN -> R.string.try_again
            CheckResult.SEE_ANSWERS -> R.string.see_answers
        }
        stringProvider.getString(stringId)
    }

    init {
        loadExerciseList()
    }

    private fun loadExerciseList() {
        disposables +=
            useCase
                .getExerciseList()
                .subscribeBy(onError = {
                    Timber.e(it)
                }, onNext = {
                    _exerciseList.value = it.exerciseList
                    _answerList.value = it.answerList
                })
    }

    enum class CheckResult {
        TRY_AGAIN, SUCCESS, SEE_ANSWERS
    }

    fun onAnswerSelected(pos: Int) {

        val lastSelectedAttachedAnswer = exerciseList.value?.let {
            it.find {
                it.attachedAnswer != null && it.attachedAnswer.viewState == WordOptionView.State.SELECTED
            }
        }
        if (lastSelectedAttachedAnswer != null) {
            exerciseList.value?.toMutableList()?.let {
                _exerciseList.value = it.map {
                    it.copy(attachedAnswer = it.attachedAnswer?.copy(viewState = WordOptionView.State.SET))
                }
            }
        }

        answerList.value?.toMutableList()?.let {
            it.map { entity ->
                entity.copy(viewState = WordOptionView.State.SET)
            }.toMutableList().apply {
                set(pos, get(pos).copy(viewState = WordOptionView.State.SELECTED))
                _answerList.value = this
            }
        }
    }

    fun onExerciseSelected(pos: Int) {

        exerciseList.value?.toMutableList()?.let { exerciseList ->

            val lastSelectedAnswer = answerList.value?.find { entity ->
                entity.viewState == WordOptionView.State.SELECTED
            }

            val lastSelectedAttachedAnswer = exerciseList.find {
                it.attachedAnswer != null && it.attachedAnswer.viewState == WordOptionView.State.SELECTED
            }

            val currentAttachedAnswer = exerciseList[pos].attachedAnswer

            if (lastSelectedAnswer != null) {
                //change selection
                if (currentAttachedAnswer != null) {
                    answerList.value?.toMutableList()?.let {
                        _answerList.value = it.map { entity ->
                            entity.copy(viewState = WordOptionView.State.SET)
                        }
                    }
                    val selectedAnswer = currentAttachedAnswer.copy(viewState = WordOptionView.State.SELECTED)
                    exerciseList[pos] = exerciseList[pos]
                        .copy(attachedAnswer = selectedAnswer)
                } else {
                    //attach answer to the exercise
                    answerList.value?.toMutableList()?.let {
                        it.remove(lastSelectedAnswer)
                        _answerList.value = it
                    }
                    val attachedAnswer = lastSelectedAnswer.copy(viewState = WordOptionView.State.SET)
                    exerciseList[pos] = exerciseList[pos].copy(attachedAnswer = attachedAnswer)
                }
            } else {
                if (lastSelectedAttachedAnswer != null) {
                    //swap answers inside exercise list
                    val tmpAnswer = exerciseList[pos].attachedAnswer
                    val lastSelectedIndex = exerciseList.indexOf(lastSelectedAttachedAnswer)
                    exerciseList[pos] = exerciseList[pos].copy(
                        attachedAnswer = lastSelectedAttachedAnswer
                            .attachedAnswer?.copy(viewState = WordOptionView.State.SET)
                    )
                    exerciseList[lastSelectedIndex] = exerciseList[lastSelectedIndex].copy(attachedAnswer = tmpAnswer)
                } else {
                    if (currentAttachedAnswer != null) {
                        //apply selection to the currently attached word
                        val selectedAnswer = currentAttachedAnswer.copy(viewState = WordOptionView.State.SELECTED)
                        exerciseList[pos] = exerciseList[pos]
                            .copy(attachedAnswer = selectedAnswer)
                    } else return
                }
            }
            //push changes
            _exerciseList.value = exerciseList
        }
    }

    fun onCheckButtonClick() {

        var wrongAnswers = 0

        exerciseList.value?.toMutableList()?.let {
            _exerciseList.value = it.mapIndexed { index, exercise ->
                if (exercise.attachedAnswer == null) {
                    wrongAnswers++
                    return@mapIndexed exercise
                }
                if (exercise.answerMatches) {
                    val answerMarkedAsRight = it[index].attachedAnswer?.copy(viewState = WordOptionView.State.MATCHED)
                    it[index].copy(attachedAnswer = answerMarkedAsRight)
                } else {
                    wrongAnswers++
                    val answerMarkedAsWrong =
                        it[index].attachedAnswer?.copy(viewState = WordOptionView.State.NOT_MATCHED)
                    it[index].copy(attachedAnswer = answerMarkedAsWrong)
                }
            }
        }

        if (wrongAnswers > 0) _checkResult.value = CheckResult.TRY_AGAIN else CheckResult.SUCCESS

        // mark all answer options left as wrong
        answerList.value?.toMutableList()?.let {
            _answerList.value = it.map {
                it.copy(viewState = WordOptionView.State.NOT_MATCHED)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}