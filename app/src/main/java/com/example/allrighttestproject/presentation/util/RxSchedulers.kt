package com.example.allrighttestproject.presentation.util

import com.example.allrighttestproject.injection.module.SCHEDULER_IO
import com.example.allrighttestproject.injection.module.SCHEDULER_MAIN_THREAD
import io.reactivex.CompletableTransformer
import io.reactivex.FlowableTransformer
import io.reactivex.Scheduler
import io.reactivex.SingleTransformer
import javax.inject.Inject
import javax.inject.Named

class RxSchedulers @Inject constructor(
    @Named(SCHEDULER_IO) private val workScheduler: Scheduler,
    @Named(SCHEDULER_MAIN_THREAD) private val mainThreadScheduler: Scheduler
) {

    fun <T> flowable(
    ): FlowableTransformer<T, T> {
        return FlowableTransformer { obs ->
            obs
                .subscribeOn(workScheduler)
                .observeOn(mainThreadScheduler)
        }
    }

    fun completable(
    ): CompletableTransformer {
        return CompletableTransformer { obs ->
            obs
                .subscribeOn(workScheduler)
                .observeOn(mainThreadScheduler)
        }
    }

    fun <T> single(
    ): SingleTransformer<T, T> {
        return SingleTransformer { obs ->
            obs
                .subscribeOn(workScheduler)
                .observeOn(mainThreadScheduler)
        }
    }

}