package com.example.allrighttestproject.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.allrighttestproject.databinding.ItemExerciseBinding
import com.example.allrighttestproject.domain.model.ExerciseViewEntity
import com.example.allrighttestproject.presentation.util.BindingViewHolder

class ExerciseAdapter(
    private val callback: DiffUtil.ItemCallback<ExerciseViewEntity>,
    private val onItemClick: (index: Int) -> Unit
) :
    ListAdapter<ExerciseViewEntity, BindingViewHolder>(callback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BindingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemExerciseBinding.inflate(inflater)
        val vh = BindingViewHolder(binding)
        with(binding) {
            root.setOnClickListener {
                if (vh.adapterPosition != RecyclerView.NO_POSITION) {
                    onItemClick(vh.adapterPosition)
                }
            }
        }
        return vh
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int, payloads: MutableList<Any>) {
        //do nothing
        super.onBindViewHolder(holder, position, payloads)
    }
}