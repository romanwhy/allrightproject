package com.example.allrighttestproject.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import com.example.allrighttestproject.R
import com.example.allrighttestproject.databinding.ActivityExerciseBinding
import com.example.allrighttestproject.domain.model.AnswerViewEntity
import com.example.allrighttestproject.domain.model.ExerciseViewEntity
import com.example.allrighttestproject.injection.factory.VmFactory
import com.example.allrighttestproject.presentation.adapter.AnswerAdapter
import com.example.allrighttestproject.presentation.adapter.ExerciseAdapter
import com.example.allrighttestproject.presentation.util.lifecycleOwner
import com.example.allrighttestproject.presentation.util.observe
import com.example.allrighttestproject.presentation.viewmodel.ExerciseViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class ExerciseActivity : AppCompatActivity() {

    private lateinit var binding: ActivityExerciseBinding

    private lateinit var exAdapter: ExerciseAdapter

    @Inject
    lateinit var vmFactory: VmFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_exercise)
        setSupportActionBar(binding.toolbar)

        val viewModel = ViewModelProviders.of(this, vmFactory).get(ExerciseViewModel::class.java)

        exAdapter = ExerciseAdapter(callback = object :
            DiffUtil.ItemCallback<ExerciseViewEntity>() {
            override fun areItemsTheSame(oldItem: ExerciseViewEntity, newItem: ExerciseViewEntity): Boolean =
                oldItem.index == newItem.index


            override fun areContentsTheSame(oldItem: ExerciseViewEntity, newItem: ExerciseViewEntity): Boolean =
                oldItem == newItem

            override fun getChangePayload(oldItem: ExerciseViewEntity, newItem: ExerciseViewEntity): Any? {
                return newItem.attachedAnswer
            }
        }) {
            viewModel.onExerciseSelected(it)
        }

        val answerAdapter = AnswerAdapter(callback = object :
            DiffUtil.ItemCallback<AnswerViewEntity>() {
            override fun areItemsTheSame(oldItem: AnswerViewEntity, newItem: AnswerViewEntity): Boolean =
                oldItem.wordIndex == newItem.wordIndex

            override fun areContentsTheSame(oldItem: AnswerViewEntity, newItem: AnswerViewEntity): Boolean =
                oldItem == newItem

            override fun getChangePayload(oldItem: AnswerViewEntity, newItem: AnswerViewEntity): Any? {
                return newItem.viewState
            }
        }) {
            viewModel.onAnswerSelected(it)
        }

        with(binding) {
            exerciseList.adapter = exAdapter
            answerList.adapter = answerAdapter
            this.lifecycleOwner = this@ExerciseActivity
            vm = viewModel
        }

        with(viewModel) {
            lifecycleOwner.observe(exerciseList) {
                it?.let {
                    exAdapter.submitList(it)
                }
            }
            lifecycleOwner.observe(answerList) {
                it?.let {
                    answerAdapter.submitList(it)
                }
            }
        }
    }
}


