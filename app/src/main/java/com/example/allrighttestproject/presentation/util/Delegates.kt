package com.example.allrighttestproject.presentation.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

class LiveDataDelegate<T>(private val mutableLiveData: MutableLiveData<T>) : ReadOnlyProperty<Any?, LiveData<T>> {
    override fun getValue(thisRef: Any?, property: KProperty<*>): LiveData<T> = mutableLiveData
}

fun <T> liveData(mutableLiveData: MutableLiveData<T>) = LiveDataDelegate(mutableLiveData)
