package com.example.allrighttestproject.presentation.util

import androidx.annotation.StringRes
import com.example.allrighttestproject.App
import javax.inject.Inject

class StringProvider @Inject constructor(private val context: App) {

    fun getString(@StringRes stringId: Int) =
        context.getString(stringId)

}