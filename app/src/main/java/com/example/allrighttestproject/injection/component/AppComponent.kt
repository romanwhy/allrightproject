package com.example.allrighttestproject.injection.component

import com.example.allrighttestproject.injection.module.ActivityBindingModule
import com.example.allrighttestproject.injection.module.ExerciseModule
import com.example.allrighttestproject.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBindingModule::class,
        ExerciseModule::class]
)
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent

    }

}