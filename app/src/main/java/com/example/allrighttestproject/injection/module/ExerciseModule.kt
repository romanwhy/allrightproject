package com.example.allrighttestproject.injection.module

import androidx.room.Room
import com.example.allrighttestproject.App
import com.example.allrighttestproject.R
import com.example.allrighttestproject.data.database.DATABASE_NAME
import com.example.allrighttestproject.data.database.ExerciseDao
import com.example.allrighttestproject.data.database.ExerciseDatabase
import com.example.allrighttestproject.data.model.MatchingExercise
import com.example.allrighttestproject.domain.interactor.ExerciseListInteractor
import com.example.allrighttestproject.domain.usecase.GetExerciseListUseCase
import dagger.Binds
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

const val SCHEDULER_MAIN_THREAD = "mainThread"
const val SCHEDULER_IO = "io"

@Module
abstract class ExerciseModule {

    @Binds
    abstract fun bindGetExerciseListUseCase(interactor: ExerciseListInteractor): GetExerciseListUseCase

    @Module
    companion object {

        @Singleton
        @JvmStatic
        @Provides
        fun provideExerciseDatabase(
            app: App, @Named("databaseName") name: String
        ): ExerciseDatabase {
            return Room.databaseBuilder(app.applicationContext, ExerciseDatabase::class.java, name)
                .build()
        }

        @JvmStatic
        @Provides
        @Named("databaseName")
        fun provideDatabaseName(): String = DATABASE_NAME

        @JvmStatic
        @Provides
        fun provideExerciseDao(db: ExerciseDatabase): ExerciseDao = db.exerciseDao()

        @JvmStatic
        @Provides
        @Named(SCHEDULER_MAIN_THREAD)
        fun provideMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

        @JvmStatic
        @Provides
        @Named(SCHEDULER_IO)
        fun provideWorkScheduler(): Scheduler = Schedulers.io()

        @JvmStatic
        @Provides
        @Named("databaseEntries")
        fun provideEntries(): List<MatchingExercise> = listOf(
            MatchingExercise(
                pictureResId = R.drawable.image_coffee,
                label = "coffee"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_cupcake,
                label = "cupcake"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_donut,
                label = "donut"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_fork,
                label = "fork"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_knife,
                label = "knife"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_pepper,
                label = "pepper"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_pizza,
                label = "pizza"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_plate,
                label = "plate"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_sandwich,
                label = "sandwich"
            ),
            MatchingExercise(
                pictureResId = R.drawable.image_knife,
                label = "strawberry"
            )
        )
    }

}
