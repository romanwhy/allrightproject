package com.example.allrighttestproject.injection.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.allrighttestproject.presentation.viewmodel.ExerciseViewModel
import javax.inject.Inject

@Suppress("UNCHECKED_CAST")
class VmFactory @Inject constructor(private val exerciseVm: ExerciseViewModel) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ExerciseViewModel::class.java))
            return exerciseVm as T
        else throw IllegalStateException("Unknown viewModel class $modelClass")
    }
}