package com.example.allrighttestproject.injection.module

import com.example.allrighttestproject.presentation.activity.ExerciseActivity
import dagger.Module
import dagger.Subcomponent
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun bindExerciseActivity(): ExerciseActivity

}