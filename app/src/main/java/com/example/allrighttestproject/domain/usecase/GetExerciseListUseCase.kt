package com.example.allrighttestproject.domain.usecase

import com.example.allrighttestproject.domain.interactor.CompleteExercise
import com.example.allrighttestproject.domain.model.ExerciseViewEntity
import io.reactivex.Flowable
import io.reactivex.Single
private const val defaultCount = 6
interface GetExerciseListUseCase {
    fun getExerciseList(count: Int = defaultCount): Flowable<CompleteExercise>
}
