package com.example.allrighttestproject.domain.model

import com.example.allrighttestproject.data.model.MatchingExercise

data class ExerciseViewEntity(
    val exercise: MatchingExercise,
    val index: Int,
    val attachedAnswer: AnswerViewEntity? = null
) {
    val answerMatches = index == attachedAnswer?.wordIndex
}