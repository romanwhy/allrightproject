package com.example.allrighttestproject.domain.interactor

import com.example.allrighttestproject.domain.model.AnswerViewEntity
import com.example.allrighttestproject.data.model.MatchingExercise
import com.example.allrighttestproject.data.repo.ExerciseRepo
import com.example.allrighttestproject.domain.model.ExerciseViewEntity
import com.example.allrighttestproject.domain.usecase.GetExerciseListUseCase
import javax.inject.Inject

data class CompleteExercise(val exerciseList: List<ExerciseViewEntity>, val answerList: List<AnswerViewEntity>)

class ExerciseListInteractor @Inject constructor(private val repo: ExerciseRepo) :
    GetExerciseListUseCase {

    private val exerciseViewMapper: (List<MatchingExercise>) -> (List<ExerciseViewEntity>) = {
        it.mapIndexed { index, matchingExercise ->
            ExerciseViewEntity(matchingExercise, index + 1, null)
        }
    }

    override fun getExerciseList(count: Int) =
        repo
            .getListOfExercises(count)
            .map(exerciseViewMapper)
            .map {
                CompleteExercise(it,
                    //shuffle answer options
                    it.map { entity ->
                        AnswerViewEntity(
                            entity.index,
                            entity.exercise.label
                        )
                    }.toMutableList().apply { shuffle() }.toList()
                )
            }

}