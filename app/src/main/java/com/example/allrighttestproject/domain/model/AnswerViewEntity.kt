package com.example.allrighttestproject.domain.model

import com.example.allrighttestproject.presentation.view.WordOptionView

data class AnswerViewEntity(
    val wordIndex: Int,
    val word: String,
    val viewState: WordOptionView.State = WordOptionView.State.SET
)